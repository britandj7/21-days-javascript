(()=>{
const SECOND = 1000;
const MINUTE = SECOND * 60;
const HOUR = MINUTE * 24
const DAY = HOUR * 24;
function setElementInnerText(id,text) {
		const elm = document.getElementById(id);
		elm.innerText = text;
}
function countDown() {
	const now = new Date().getTime();
	const newYear = new Date('December 31, 2020 23:59:59').getTime();
	const unixTimeLeft = newYear - now;
	// console.log(unixTimeLeft,'unixTimeLeft');
				setElementInnerText('days',Math.floor(unixTimeLeft/DAY));
				setElementInnerText('hours',Math.floor(unixTimeLeft % DAY/HOUR));
				setElementInnerText('minutes',Math.floor(unixTimeLeft % HOUR/MINUTE));
				setElementInnerText('seconds',Math.floor(unixTimeLeft % MINUTE/SECOND));


}

function run() {
		countDown()
		setInterval(() => {
			countDown()
		}, SECOND);
}
run();
})();