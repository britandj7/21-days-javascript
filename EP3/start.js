(  ()=>{
	//  function simmulateAsyncAPI(text,timeout,callback) {
	// 		setTimeout(() => {
	// 			console.log(text);
	// 			callback();
	// 		}, timeout);
	//  }
	/////////////////// promise ///////////////////////////////
	//  function simmulateAsyncAPI(text,timeout) {
	// 		return new Promise ((resolve,reject)=>{
	// 			setTimeout(() => {
	// 				console.log(text);
	// 				resolve();
	// 			}, timeout);
	// 		})
	//  }

	//  simmulateAsyncAPI('aaa',3000)
	//  .then(()=>{
	//  		console.log('end aaa');
	// 		return simmulateAsyncAPI('bbb',2000)
	//  })
	//  .then(()=>{
	//  		console.log('end bb');
	// 		return simmulateAsyncAPI('cccc',2000)	 
	//  })
	//////////////////////////////////////////////////
	 function simmulateAsyncAPI(text,timeout) {
			return new Promise ((resolve,reject)=>{
				setTimeout(() => {
					if(text === 'B') return reject('REJECT B')
					console.log(text);
					resolve();
				}, timeout);
			})
	 }

	 async function run() {
		 		await simmulateAsyncAPI('A',2000)
		 		await simmulateAsyncAPI('B',1500)
		 		await simmulateAsyncAPI('C',1000)
	 }
	 run()
	//  simmulateAsyncAPI('bbb',2000)
	//  simmulateAsyncAPI('ccc',1000)

})();